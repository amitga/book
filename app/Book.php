<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
       //database fields to be allowed for massive assignment
       protected $fillable=[
        'title', 'author', 'status'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
