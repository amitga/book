@yield('content')

@extends('layouts.app')

@section('content')

<h1>Add a Book</h1>

<form method = 'post' action = "{{action('BookController@store')}}" >

{{csrf_field()}}

<div class = "form-group">
<label for = "title" > Title </label>
<br>
<input type = "text" class = "form-control" name = "title">
<br>
<p>author:</p>
<input type = "text" class = "form-control" name = "author">
</div>

<div class = "form-group">
<input type = "submit" class = "form-control" name = "submit" value = "Save">
</div>

</form>

@endsection