@yield('content')

@extends('layouts.app')

@section('content')

<h1> This is your book list</h1>

<table>
    <tr>
    <th>Book Name</th>
    <th>Author</th>
    <th>status</th>
    </tr>
    @foreach($books as $book)
    <tr>
    <td><a href="{{route('books.edit',$book->id)}}"> {{$book->title}} </a></td>
    <td>{{$book->author}}</td>
    <td> @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" disabled='disable' checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif </td>
    </tr>
    @endforeach
    </table>

    <a href = "{{route('books.create')}}"> Create a new book</a>

    <script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $(this).attr('disabled', true);
               console.log(event.target.id)
               $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json', 
                   type: 'put', 
                   contentType: 'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  



    <script>
    /*     $(document).ready(function(){
           $(":chekbox").click(function(event){
                console.log(event.target.id)
                $.ajax({
                   url: "{{url('books')}}" + '/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData: false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>
     

    <style>
table, th, td {
  border: 1px solid black;
}
    </style>


@endsection


@if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif